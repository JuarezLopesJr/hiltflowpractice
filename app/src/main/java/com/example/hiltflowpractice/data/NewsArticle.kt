package com.example.hiltflowpractice.data

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@ExperimentalSerializationApi
@Serializable
data class NewsArticle(
    val author: String = "",
    val title: String = "",
    @SerialName("desription")
    val description: String = "",
    val url: String = "",
    @SerialName("imageUrl")
    val urlToImage: String = "",
    val publishedAt: String = ""
)