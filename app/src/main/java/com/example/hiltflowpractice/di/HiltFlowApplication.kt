package com.example.hiltflowpractice.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class HiltFlowApplication : Application()