package com.example.hiltflowpractice.repository

import com.example.hiltflowpractice.data.NewsArticle
import kotlinx.serialization.ExperimentalSerializationApi
import retrofit2.http.GET

@ExperimentalSerializationApi
interface NewsApi {
    @GET("news.json")
    suspend fun getNews(): List<NewsArticle>
}