package com.example.hiltflowpractice.repository

import com.example.hiltflowpractice.data.NewsArticle
import kotlinx.coroutines.flow.Flow
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
interface NewsRemoteDataSource {
    fun getNewsArticle(): Flow<NewsArticle>
}