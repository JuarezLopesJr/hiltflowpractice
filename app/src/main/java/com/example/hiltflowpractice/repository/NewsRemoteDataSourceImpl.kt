package com.example.hiltflowpractice.repository

import com.example.hiltflowpractice.data.NewsArticle
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
class NewsRemoteDataSourceImpl(private val newsApi: NewsApi) : NewsRemoteDataSource {
    override fun getNewsArticle(): Flow<NewsArticle> {
        return flow {
            val newsSource = newsApi.getNews()
            newsSource.forEach {
                emit(it)
                delay(2000L)
            }
        }
    }
}