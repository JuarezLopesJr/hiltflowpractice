package com.example.hiltflowpractice.ui.screens

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.ExperimentalLifecycleComposeApi
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.hiltflowpractice.R
import com.example.hiltflowpractice.data.NewsArticle
import com.example.hiltflowpractice.viewmodel.ListViewModel
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalLifecycleComposeApi
@ExperimentalSerializationApi
@Composable
fun Home(
    modifier: Modifier = Modifier,
    newsViewModel: ListViewModel = hiltViewModel()
) {
    val newsArticles by
    newsViewModel.newsArticles.collectAsStateWithLifecycle(NewsArticle())

    LazyColumn(
        modifier = modifier,
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        contentPadding = PaddingValues(16.dp)
    ) {
        items(count = 20) {

            AsyncImage(
                model = ImageRequest.Builder(LocalContext.current)
                    .data(newsArticles.urlToImage)
                    .crossfade(true)
                    .build(),
                placeholder = painterResource(id = R.drawable.ic_launcher_foreground),
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(CircleShape),
                contentDescription = newsArticles.author
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(
                text = newsArticles.author,
                fontSize = MaterialTheme.typography.headlineLarge.fontSize,
                fontWeight = FontWeight.Bold
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(
                text = newsArticles.title,
                fontSize = MaterialTheme.typography.titleMedium.fontSize,
                fontWeight = FontWeight.Bold
            )

            Spacer(modifier = Modifier.height(20.dp))

            Text(text = newsArticles.description)
        }
    }
}