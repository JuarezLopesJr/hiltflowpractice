package com.example.hiltflowpractice.viewmodel

import androidx.lifecycle.ViewModel
import com.example.hiltflowpractice.repository.NewsRemoteDataSource
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
@HiltViewModel
class ListViewModel @Inject constructor(
    dataSource: NewsRemoteDataSource
) : ViewModel() {
    val newsArticles = dataSource.getNewsArticle()
}